<?php

require_once dirname(__FILE__) . '/vendor/swiftmailer/swiftmailer/lib/swift_required.php';

function clean($var)    {
    $var = strip_tags($var);
    $var = trim($var);
    return $var;
}

$adminemail = "info@protection4u.nu";

foreach($_POST as $key=>$value) {
    $_POST[$key] = clean($value);
}

if(isset($_POST['submitf']) && $_POST['submitf'] == 'submit' && empty($_POST['emailname'])) {
    $_uname = $_POST['name'];
    $_uemail = $_POST['email'];
    $_phone = $_POST['phone'];
    $_website = $_POST['website'];
    $_comments = stripslashes($_POST['comment']);
    $email_check = '';
    $return_arr = [];
    if($_uname == "" || $_uemail == "" || $_comments == "") {
        $return_arr["frm_check"] = 'error';
        $return_arr["msg"] = "Please fill in all required fields!";
    }
    else {
        if(filter_var($_uemail, FILTER_VALIDATE_EMAIL)) {

            $transport = Swift_SmtpTransport::newInstance('smtp.mailgun.org', 587, 'tls')
                ->setUsername('postmaster@email.protection4u.nu')
                ->setPassword('28a590d444f6cc977d95f2bf65897332')
            ;

            $mailer = Swift_Mailer::newInstance($transport);

            // Create the message
            $message = Swift_Message::newInstance($transport)
                ->setSubject("Protection4u Email: " . $_uname)
                ->setFrom(array($_uemail => $_POST['name']))
                ->setTo(array($adminemail))
                ->setBody('Name: &nbsp;&nbsp;' . $_uname . '<br><br> Email: &nbsp;&nbsp;' . $_uemail . '<br><br> Phone: &nbsp;&nbsp;' . $_phone . '<br><br> Website: &nbsp;&nbsp;' . $_website . '<br><br> Comment:&nbsp;&nbsp;' . $_comments, 'text/html')
            ;
            $result = $mailer->send($message);
            if($result) {
                $return_arr['frm_check'] = 'success';
            }else{
                $return_arr['frm_check'] = 'fail';
            }
        }
        else {
            $return_arr["frm_check"] = 'error';
            $return_arr["msg"] = "Please enter a valid email address!";
        }
    }
}

header('Location: /contact.php?frm_check=' . $return_arr['frm_check']);
?>
