<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Protection4u | Contact</title>
		<!-- All Stylesheets -->
		<link href="css/all-stylesheets.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="contact-us">
		<!-- NAVIGATION STARTS
			========================================================================= -->
		<nav id="navigation">
			<div id="tc" class="container top-contact hidden-sm hidden-xs">
				<div class="row">
					<div class="col-lg-2 col-md-2">
						<!--  Logo Starts -->
						<a class="navbar-brand external" href="index.html"><img src="images/logos/logo-1.png" alt=""></a>
						<!-- Logo Ends -->
					</div>
					<div class="col-lg-10 col-md-10">
						<ul class="small-nav">
							<li><a href="0858009000"><i class="fa fa-phone-square"></i> 085-8009000 </li>
							<li><a href="mailto:info@protection4u.nu>"><i class="fa fa-envelope-square"></i> info@protection4u.nu</a></li>
							<li class="shop">
								<a href="#" target="_blank"><i class="fa fa-shopping-bag"></i></a>
							</li>
							<li class="searchlink" id="searchlink">
								<div class="searchform">
									<form id="search-1">
										<input type="text" class="s" id="s-1" placeholder="keywords...">
										<button type="submit" class="sbtn"><i class="fa fa-search"></i></button>
									</form>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="navbar navbar-inverse" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
						<!--  Logo Starts -->
						<a class="navbar-brand external visible-xs" href="index.html"><img src="images/logos/footer-logo.png" alt=""></a>
						<!-- Logo Ends -->
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.html" class="external">HOME</a></li>
							<li><a href="over.html" class="external">OVER ONS</a></li>
							<li><a href="services.html" class="external">SERVICES</a></li>
							<li><a href="contact.php" class="external">CONTACT</a></li>
							<li><a href="http://protection4u.shop/" target="_blank" class="external" style="color: #ff7f2f;"><i class="fa fa-shopping-bag" style="padding-right:10px;"></i> WEBSHOP</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right nav-social-icons hidden-xs">
							<li><a href="https://www.facebook.com/Protection-4U-189154521489223/?fref=ts"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://www.linkedin.com/company/1325304?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A1325304%2Cidx%3A1-1-1%2CtarId%3A1481586965864%2Ctas%3Aprotection4u" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</nav>
		<!-- /. NAVIGATION ENDS
			========================================================================= -->
				<!-- INNER BANNER STARTS
			========================================================================= -->
		<div class="inner-banner-style banner-img-02">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="info">
							<h1>Contact ons</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /. INNER BANNER ENDS
			========================================================================= -->
		<!-- ADDRESS STARTS
			========================================================================= -->
		<div class="container address">
			<div class="row no-gutter no-gutter-5 no-gutter-6">
				<div class="col-lg-5 col-md-5 col-sm-5">
					<div class="herotext overlay-margin">
						<h1>Onze vestiging</h1><br>
						<div class="description">Protection 4 U is dé online specialist op het gebied van beveiliging. Bij ons schaf je geen duur alarmsysteem aan maar alleen hoogwaardige producten om je huis of pand op de juiste manier te beveiligen.</div>
						<div class="line"></div>
						<ul class="bullets-contact">
							<li><i class="fa fa-phone-square"></i>085-8009000</li>
							<li><a href="mailto:info@companyname.com"><i class="fa fa-envelope-square"></i>info@ptorectionforyou.nl</a></li>
							<li><i class="fa fa-home"></i>Zuidergracht 21-28, 3763 LS Soest</li>
							<li><i class="fa fa-clock-o"></i>Maandag t/m vrijdag 09:00 - 17:00</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                    <?php
                    if(isset($_GET['frm_check']) && $_GET['frm_check']=='success')    {
                        echo '<div class="alert-success" style="margin-bottom: 40px; padding: 10px; margin-top: -40px;">Uw aanvraag wordt zo spoedig mogelijk behandeld.</div>';
                    }

                    if(isset($_GET['frm_check']) && $_GET['frm_check']=='fail')    {
                        echo '<div class="alert-danger" style="margin-bottom: 40px; padding: 10px; margin-top: -40px;">Kon aanvraag niet versturen.</div>';
                    }
                    ?>
					<div class="google-map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2446.745814581749!2d5.26364641579462!3d52.175314779750344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c641e6a4774acf%3A0x51c888d51f8ad3e8!2sZuidergracht+2128%2C+3763+LS+Soest!5e0!3m2!1snl!2snl!4v1473774901886" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
		<!-- /. ADDRESS ENDS
			========================================================================= -->
		<!-- SEND US MESSAGE START
			========================================================================= -->
		<div class="send-us-message white-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h1>Neem contact met ons op</h1>
						<div class="line"></div>
						<!-- Form Starts -->
						<form action='process.php' method='post' name='ContactForm' id='ContactForm' >
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" name="name" placeholder="Naam *">
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<input type="email" class="form-control" name="email" placeholder="E-mailadres*">
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" name="phone" placeholder="Telefoonnummer">
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" name="website" placeholder="Bedrijf">
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<textarea rows="5" class="form-control" name="comment" placeholder="Bericht*" style="height:150px;"></textarea>
									</div>
								</div>
								<div class="col-lg-12 butn">
									<div id='message_post'></div>
									<button type="submit" class="btn btn-default" name='submitf' id="submitf" value="submit">Bericht verzenden <i class="fa fa-chevron-right"></i></button>
                                    <input type="text" style="display:none;" name="emailname" />
								</div>
							</div>
						</form>
						<!-- Form Ends -->
					</div>
				</div>
			</div>
		</div>
		<!-- /. SEND US MESSAGE ENDS
			========================================================================= -->
		<!-- DOWNLOAD STARTS
			========================================================================= -->
		<!-- <div class="download-brochure parallax-4">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-7 col-lg-offset-1">
						<h1>Download de brochure</h1>
						<h2>Maak een keuze uit alle modellen</h2>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-4">
						<div class="button"><a href="#" target="_blank"><i class="fa fa-file-pdf-o"></i> Brouchure 2016</a></div>
					</div>
				</div>
			</div>
		</div> -->
		<!-- /. DOWNLOAD ENDS
			========================================================================= -->
			<!-- FOOTER STARTS
				========================================================================= -->
				<footer class="dark-grey-bg">
					<div class="container">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-6">
								<img src="images/logos/footer-logo.png" alt="">
								<ul class="contact">
									<li><i class="fa fa-phone-square"></i><a href="tel:0031615247116"></a>085-8009000</li>
									<li><a href="mailto:info@protectionforyou.nl"><i class="fa fa-envelope-square"></i> info@protectionforyou.nl</a></li>
									<li><i class="fa fa-home"></i> Zuidergracht 21-28, 3763 LS Soest</li>
								</ul>
							</div>

							<div class="col-lg-2 col-md-2 col-sm-6">
								<h1>Protection4u</h1>
								<div class="line2"></div>
								<ul class="usefullinks">
									<li><a href="index.html">Home</a></li>
									<li><a href="over.html">Over ons</a></li>
									<li><a href="services.html">Services</a></li>
								</ul>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6">

								<div class="line2" style="margin-top:48px;"></div>
								<ul class="usefullinks">
									<li><a href="contact.php">Contact</a></li>
									<li><a href="http://protection4u.shop/" target="_blank">Webshop</a></li>



								</ul>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6">
								<h1>Connect</h1>
								<div class="line2"></div>
								<ul class="connect">
									<li><a href="https://www.facebook.com/Protection-4U-189154521489223/?fref=ts"><i class="fa fa-facebook"></i> Facebook</a></li>
									<li><a href="https://www.linkedin.com/company/1325304?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A1325304%2Cidx%3A1-1-1%2CtarId%3A1481586965864%2Ctas%3Aprotection4u"><i class="fa fa-linkedin"></i> LinkedIn</a></li>
								</ul>
							</div>
						</div>
						<div class="row copyright">
							<div class="col-lg-12">
								Copyrights 2016  <span>Protection4u</span>  All rights reserved
							</div>
						</div>
					</div>
				</footer>
			<!-- /. FOOTER ENDS
				========================================================================= -->
			<!-- TO TOP STARTS
				========================================================================= -->
			<a href="#" class="scrollup">Scroll</a>
			<!-- /. TO TOP ENDS
				========================================================================= -->
			<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
			<!-- Include all compiled plugins (below), or include individual files as needed -->
			<script src="js/bootstrap.min.js"></script>
			<!-- Overlay Resize Menu -->
			<script src="js/overlay-resizemenu/js/classie.js"></script>
			<script src="js/overlay-resizemenu/js/hide-top-row.js"></script>
			<!-- REVOLUTION JS FILES -->
			<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
			<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
			<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
				(Load Extensions only on Local File Systems !
				The following part can be removed on Server for On Demand Loading) -->
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
			<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
			<script type="text/javascript">
				var tpj=jQuery;
				var revapi4;
				tpj(document).ready(function() {
					if(tpj("#rev_slider_46_1").revolution == undefined){
						revslider_showDoubleJqueryError("#rev_slider_46_1");
					}else{
						revapi4 = tpj("#rev_slider_46_1").show().revolution({
							sliderType:"standard",
							jsFileLocation:"revolution/js/",
							sliderLayout:"fullscreen",
							dottedOverlay:"none",
							delay:9000,
							navigation: {
								keyboardNavigation:"off",
								keyboard_direction: "horizontal",
								mouseScrollNavigation:"off",
								onHoverStop:"off",
								touch:{
									touchenabled:"on",
									swipe_threshold: 75,
									swipe_min_touches: 1,
									swipe_direction: "horizontal",
									drag_block_vertical: false
								}
								,


							},
							viewPort: {
								enable:true,
								outof:"pause",
								visible_area:"80%"
							},
							responsiveLevels:[1240,1024,778,480],
							gridwidth:[1240,1024,778,480],
							gridheight:[600,600,500,400],
							lazyType:"none",
							parallax: {
								type:"mouse",
								origo:"slidercenter",
								speed:2000,
								levels:[2,3,4,5,6,7,12,16,10,50],
							},
							shadow:0,
							spinner:"off",
							stopLoop:"off",
							stopAfterLoops:-1,
							stopAtSlide:-1,

							fullScreenAlignForce:"off",
							fullScreenOffsetContainer: "",
							fullScreenOffset: "0px",
							disableProgressBar:"on",
							hideThumbsOnMobile:"off",

							shuffle:"off",
							autoHeight:"off",
							hideThumbsOnMobile:"off",
							hideSliderAtLimit:0,
							hideCaptionAtLimit:0,
							hideAllCaptionAtLilmit:0,
							debugMode:false,
							fallbacks: {
								simplifyAll:"off",
								nextSlideOnWindowFocus:"off",
								disableFocusListener:false,
							}
						});
					}
				});	/*ready*/
			</script>
			<!-- Isotope Gallery -->
			<script type="text/javascript" src="js/isotope/jquery.isotope.min.js"></script>
			<script type="text/javascript" src="js/isotope/custom-isotope-mansory.js"></script>
			<!-- Magnific Popup core JS file -->
			<script type="text/javascript" src="js/magnific-popup/jquery.magnific-popup.js"></script>
			<!-- Owl Carousel -->
			<script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
			<!-- FitVids -->
			<script type="text/javascript" src="js/fitvids/jquery.fitvids.js"></script>
			<!-- ScrollTo -->
			<script type="text/javascript" src="js/nav/jquery.scrollTo.js"></script>
			<script type="text/javascript" src="js/nav/jquery.nav.js"></script>
			<!-- Sticky -->
			<script type="text/javascript" src="js/sticky/jquery.sticky.js"></script>
			<!-- Custom JS -->
			<script src="js/custom/custom.js"></script>
		</body>
	</html>
